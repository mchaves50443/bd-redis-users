
 const redis = require("redis");

 /**
  *Unused at the moment
  */
 const UserSchema = redis.Schema(
   {
     name: String,
     description: String,
     price: Number,
     status: String,
   },
   {
     timestamps: true,
   }
 );
 
 module.exports = redis.model("Product", UserSchema);
 